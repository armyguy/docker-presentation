var express   =    require("express");
var mysql     =    require('mysql');
var app       =    express();

var pool      =    mysql.createPool({
    connectionLimit : 100, //important
    host     : 'mysql-server',
    user     : 'user',
    password : 'user',
    database : 'example',
    debug    :  false
});

//
// -----------------------------------------------------------------------------
//

function create_database(req,res) {
    
    pool.getConnection(function(err,connection){
        if (err) {
          connection.release();
          res.json({"code" : 100, "status" : "Error in connection database"});
          return;
        }   
        connection.query("CREATE TABLE IF NOT EXISTS `test` ( id int auto_increment, text varchar(100), primary key (id));",function(err,rows){
            connection.release();
            if(!err) {
                console.log("Creating a table if it is not exists!")
                res.json(rows);
            }           
        });
        connection.on('error', function(err) {      
              res.json({"code" : 100, "status" : "Error in connection database"});
              return;     
        });
  });
}

function records_count(req,res) {
    pool.getConnection(function(err,connection){
        connection.query("SELECT count(*) as records_count FROM test;",function(err,rows){
            connection.release();
            if(!err) {            
                console.log("Counting of records number.");    
                res.json(rows);
            }           
        });
    })
}

function insert(req,res) {
    pool.getConnection(function(err,connection){
        connection.query("INSERT INTO test (text) VALUES ('test-message');",function(err,rows){
            connection.release();
            if(!err) {                
                res.json(rows);
                console.log("Inserting a record into the table")
            }           
        });
    })
}

//
// -----------------------------------------------------------------------------
//
app.get("/",function(req,res){-
        create_database(req,res);
});

app.get("/records_count",function(req,res){-
    records_count(req,res);
});

app.get("/insert",function(req,res){-
    insert(req,res);
})

console.log('Server running at http://127.0.0.1:8080/');

app.listen(8080);

