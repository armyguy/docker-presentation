###Prepearing to the presentation
```

$ vagrant up
$ vagrant ssh
$ cd /vagrant/docker-compose

# Preparing the nodejs-project
$ docker-compose run --rm --no-deps node-server bash -c "cd /nodejs-project; npm install;"

# Launching the nodejs-project (with MySQL)
$ docker-compose up -d 

# Creating a table
$ curl 127.0.0.1:8080/

# Getting a records count
$ curl 127.0.0.1:8080/records_count

# Inserting a new record into the table
$ curl 127.0.0.1:8080/insert

```

###
Here is available **phpmyadmin** by url: http://presentation/phpmyadmin to get access to MySQL, that was runned in the container

###Destroy the vagrant machine after presentation
```
$ vagrant halt
$ vagrant destroy
```

### Attention! 
On Windows hosts you have to clone this project inside the vagrant's home directory, because npm install can be interrupted. 
There are some problems with rights. On Linux's hosts such problems are not presented