# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

hostname = "presentation"
ip = "192.168.60.10"
ram = "2048"

Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/trusty64"
  config.vm.hostname = "#{hostname}"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "#{ip}"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.

  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.aliases = %w(presentation)

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
   config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
     vb.memory = "#{ram}"
   end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
   config.vm.provision "shell", inline: <<-SHELL
        cp /etc/apt/sources.list /etc/apt/sources.list.orig
        sed -i "s/archive/ua.archive/g" /etc/apt/sources.list
        # Set our native time zone in VM
        ln -sf /usr/share/zoneinfo/Europe/Kiev /etc/localtime
        # mc repo
        echo "deb http://www.tataranovich.com/debian trusty nightly" > /etc/apt/sources.list.d/mc.list
        apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 76FB442E
        # Docker repo
        apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
        echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" > /etc/apt/sources.list.d/docker.list
        apt-get update
        #
        apt-get -y install linux-image-extra-$(uname -r)
        #
        apt-get install -y \
            mc \
            htop \
            docker-engine \
            python-pip \
            apache2 \
            php5 php5-cli php5-mysql
            
        #
        usermod -aG docker vagrant
        echo "ServerName #{hostname}" >> /etc/apache2/apache2.conf        
        service apache2 restart
        #
        # phpmyadmin
        #
        if [ ! -e "/var/www/other/phpmyadmin" ]; then
            mkdir -p /var/www/other
            cd /var/www/other
            curl https://files.phpmyadmin.net/phpMyAdmin/4.6.0/phpMyAdmin-4.6.0-all-languages.tar.gz | tar -xvzf -
            mv phpMyAdmin-4.6.0-all-languages phpmyadmin
            cp phpmyadmin/config.sample.inc.php phpmyadmin/config.inc.php
            sed -i "s/blowfish_secret'\] = ''/blowfish_secret'\] = 'bhNgoThie4phoh5a'/g" /var/www/other/phpmyadmin/config.inc.php
            sed -i "s/localhost/127.0.0.1/g" /var/www/other/phpmyadmin/config.inc.php
            sed -i "/DocumentRoot/aAlias \"\/phpmyadmin\" \"/var/www/other/phpmyadmin\"" /etc/apache2/sites-available/000-default.conf            
            service apache2 restart
        fi
        #
        #
        echo "Getting docker-compose from the official repository..."
        if [ ! -e "/usr/local/bin/docker-compose" ]; then
            curl -sL https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
            chmod +x /usr/local/bin/docker-compose
        else
            echo "Skip this step, because docker-compose is already exist!"
        fi
   SHELL
end
